<?php

declare(strict_types=1);

namespace App\Presenters;

use App\Model\OlmpManager;
use App\Model\StrucManager;
use App\Model\OlmpManager2;
use App\Model\StrucManager2;
use Nette;
use Tracy\Debugger;


final class HomepagePresenter extends BasePresenter
{

    private $strucManager;
    private $olmpManager;

    //public $structure = 1;
    public $structure = 2;

    public $tablesNames = [];

    public function __construct(StrucManager $strucManager, OlmpManager $olmpManager, StrucManager2 $strucManager2, OlmpManager2 $olmpManager2)
    {
        if ($this->structure == 1) {
            $this->strucManager = $strucManager;
            $this->olmpManager = $olmpManager;
            $this->tablesNames = ['bazen', 'clovek', 'obdobi', 'plavec', 'prihlaska', 'skupina', 'termin', 'vykon_kategorie'];
        } elseif ($this->structure == 2) {
            $this->strucManager = $strucManager2;
            $this->olmpManager = $olmpManager2;
            $this->tablesNames = ['text'];
        }
    }

    public function renderDefault()
    {
        $strucMan = $this->strucManager->database;
        $olmpMan = $this->olmpManager->database;
        /*$strucMan->beginTransaction();
        $olmpMan->beginTransaction();*/
        //Debugger::barDump( $this->strucManager, ' $this->strucManager');
        $tablesNames = $this->tablesNames;
        /*foreach ($tablesNames as $name) {
            $this->loadCell($name); //DEPR
        }*/
        $this->loadBaseStructure();
        $this->deleteDuplicates();
        $this->calculateSizes();
        $this->calculateHigherLevelSizes(3);
        $this->calculateHigherLevelSizes(2);
        $this->calculateHigherLevelSizes(1);
        $this->addDataReferences();
        $this->calculateHierarchicallity($tablesNames);
        $this->calculateStructuredness(4);
        $this->calculateStructuredness(3);
        $this->calculateStructuredness(2);
        $this->calculateStructuredness(1);
        $this->calculateInformationAmount(4);
        $this->calculateInformationAmount(3);
        $this->calculateInformationAmount(2);
        $this->calculateInformationAmount(1);

        /*$strucMan->commit();
        $olmpMan->commit();*/
        $this->template->result = null;
        echo "COMPLETED";
    }

    public function calculateInformationAmount(int $level)
    {
        $strucMan = $this->strucManager->database;
        $olmpMan = $this->olmpManager->database; //structuredness IS NULL AND
        $rows = $olmpMan->table('data_object')->where('information_amount IS NULL AND ROUND (   
        (
            LENGTH(location_specification)
            - LENGTH( REPLACE ( location_specification, "/", "") ) 
        ) / LENGTH("/")  = ?)', $level);
        echo count($rows) . " rows found<br>";
        foreach ($rows as $row) {
            $location = $row->location_specification;
            if ($level == 4) {
                preg_match('/\/(.*)\/(.*)\/(.*)\//', $location, $matches);
                //Debugger::barDump($matches, '$matches');
                $tableName = $matches[1];
                $id = $matches[2];
                $column = $matches[3];
                $value = $strucMan->table($tableName)
                    ->get($id)
                    ->$column;
                if ($value instanceof \DateInterval) {
                    $value = $value->format("%Y-%M-%D %H:%I:%S");
                }
                $informationAmount = $this->calculateStringInformationAmount("$value");
                $row->update([
                    'information_amount' => $informationAmount
                ]);
                echo "updated information_amount lvl4 $informationAmount for $location - '$value'  <br>";
            }
            if ($level == 3) {
                preg_match('/\/(.*)\/(.*)\//', $location, $matches);
                //Debugger::barDump($matches, '$matches');
                $tableName = str_replace(' ', '', $matches[1]);
                $id = str_replace(' ', '', $matches[2]);
                $value = '';
                $columns = $strucMan->table($tableName)->get($id)->toArray();
                foreach ($columns as $column) {
                    if ($column instanceof \DateInterval) {
                        $column = $column->format("%Y-%M-%D %H:%I:%S");
                    }
                    $value .= $column;
                }
                $informationAmount = $this->calculateStringInformationAmount("$value");
                $row->update([
                    'information_amount' => $informationAmount
                ]);
                echo "updated information_amount lvl3 $informationAmount for $location <br>";
            }
            if ($level == 2) {
                preg_match('/\/(.*)\//', $location, $matches);
                $tableName = str_replace(' ', '', $matches[1]);
                echo "TABLE $tableName<br>";
                $array = $strucMan->table($tableName)->fetchAll();
                $value = '';
                foreach ($array as $tableRow) {
                    foreach ($tableRow as $column) {
                        if ($column instanceof \DateInterval) {
                            $column = $column->format("%Y-%M-%D %H:%I:%S");
                        }
                        //echo $column."<br>";
                        $value .= $column;
                    }
                }
                $informationAmount = $this->calculateStringInformationAmount("$value");
                $row->update([
                    'information_amount' => $informationAmount
                ]);
                echo "updated information_amount lvl2 $informationAmount for $location <br>";
            }
            if ($level == 1) {
                $value = '';
                foreach ($this->tablesNames as $tableName) {
                    $array = $strucMan->table($tableName)->fetchAll();
                    foreach ($array as $tableRow) {
                        foreach ($tableRow as $column) {
                            if ($column instanceof \DateInterval) {
                                $column = $column->format("%Y-%M-%D %H:%I:%S");
                            }
                            //echo $column."<br>";
                            $value .= $column;
                        }
                    }
                }
                $informationAmount = $this->calculateStringInformationAmount("$value");
                $row->update([
                    'information_amount' => $informationAmount
                ]);
                echo "updated information_amount lvl1 $informationAmount for $location <br>";
            }
        }
    }

    public function calculateStringInformationAmount(string $string): float
    {
        echo "calculateStringInformationAmount<br>";
        $originalLength = strlen($string);
        if ($originalLength == 0) {
            echo "0 length<br>";
            return 1;
        }
        $compressedLength = strlen(gzencode($string, 9));
        if ($compressedLength > $originalLength) {
            echo "$compressedLength>$originalLength<br>";
            return 1;
        }
        echo "$compressedLength / $originalLength = " . ($compressedLength / $originalLength) . "<br>";
        return $compressedLength / $originalLength;
    }

    public function calculateCellsStructuredness(array $tablesNames)
    {
        $strucMan = $this->strucManager->database;
        $olmpMan = $this->olmpManager->database;
        /*foreach ($tablesNames as $tableName) {
            $table = $strucMan->table($tableName);
            foreach ($table as $row) {
                $idColumn = "id_$tableName";
                $id = $row->$idColumn;
                foreach ($row as $key => $value) {
                    if ($value instanceof \DateInterval) {
                        $value = $value->format("%Y-%M-%D %H:%I:%S");
                    }
                    $structuredness = substr_count("$value", " ") > 1 ? 0 : 1;
                    $olmpMan->table('data_object')->where('location_specification', "/$tableName/$id/$key/")->fetch()
                        ->update([
                            'structuredness' => $structuredness
                        ]);
                    echo "updated structuredness $structuredness for /$tableName/$id/$key/ - '$value'  <br>";
                }
            }
        }*/
    }


    public function calculateStructuredness(int $level)
    {
        $strucMan = $this->strucManager->database;
        $olmpMan = $this->olmpManager->database; //structuredness IS NULL AND
        $rows = $olmpMan->table('data_object')->where('structuredness IS NULL AND ROUND (   
        (
            LENGTH(location_specification)
            - LENGTH( REPLACE ( location_specification, "/", "") ) 
        ) / LENGTH("/")  = ?)', $level);
        //echo "calculateHigherLevelSizes level $level found uncalc " . count($rows)."<br>";
        echo count($rows) . " rows found<br>";
        foreach ($rows as $row) {
            $location = $row->location_specification;
            if ($level == 4) {
                preg_match('/\/(.*)\/(.*)\/(.*)\//', $location, $matches);
                //Debugger::barDump($matches, '$matches');
                $tableName = $matches[1];
                $id = $matches[2];
                $column = $matches[3];
                $value = $strucMan->table($tableName)
                    ->get($id)
                    ->$column;
                if ($value instanceof \DateInterval) {
                    $value = $value->format("%Y-%M-%D %H:%I:%S");
                }
                $structuredness = substr_count("$value", " ") > 1 ? 0 : 1;
                $row->update([
                    'structuredness' => $structuredness
                ]);
                echo "updated structuredness lvl4 $structuredness for $location - '$value'  <br>";
                continue;
            }
            $DSize = $row->size;
            $parts = $olmpMan->table('data_object')
                ->where('parent_object_id', $row->object_id);
            //Debugger::barDump($parts->fetchAll());
            //Debugger::barDump($row);
            $sum = $parts->sum("(1-((size/$DSize)*(1-structuredness)))*size");
            $structuredness = $sum / $DSize;
            //echo "$structuredness = $sum / $DSize;<br>";
            $olmpMan->table('data_object')->where('location_specification', $location)->fetch()
                ->update([
                    'structuredness' => $structuredness
                ]);
            //$row->update(['structuredness' => $structuredness]);
            echo "updated structuredness $structuredness as $sum / $DSize at lvl$level for $location<br>";
            //echo $row->location_specification . "structuredness $structuredness calculated<br>";
        }
    }

    public function loadBaseStructure()
    {
        $strucMan = $this->strucManager->database;
        $olmpMan = $this->olmpManager->database;$strucMan->beginTransaction();
        /*$strucMan->rollBack();
        $olmpMan->rollBack();*/
        $olmpMan->beginTransaction();
        //LVL1
        $root = $olmpMan->table('data_object')->insert([
            'parent_object_id' => null,
            'location_specification' => "/",
            'existence_from' => '2019-03-01 00:00:00'
        ]);
        echo "ROOT<br>";
        $tablesNames = $this->tablesNames;
        foreach ($tablesNames as $tableName) {
            //LVL2
            echo "TABLE $tableName<br>";
            $table = $olmpMan->table('data_object')->insert([
                'parent_object_id' => $root->object_id,
                'location_specification' => "/$tableName/",
                'existence_from' => '2019-03-01 00:00:00'
            ]);
            $rowsData = $strucMan->table($tableName);
            foreach ($rowsData as $rowData){
                //LVL3
                $idRowName = "id_$tableName";
                $idRow = $rowData->$idRowName;
                echo "TABLE $tableName/$idRow<br>";
                $row = $olmpMan->table('data_object')->insert([
                    'parent_object_id' => $table->object_id,
                    'location_specification' => "/$tableName/$idRow/",
                    'existence_from' => '2019-03-01 00:00:00'
                ]);
                $rowValues = $strucMan->table($tableName)->get($idRow)->toArray();
                foreach ($rowValues as $key => $value){
                    //LVL4
                    echo "TABLE $tableName/$idRow/$key<br>";
                    $cell = $olmpMan->table('data_object')->insert([
                        'parent_object_id' => $row->object_id,
                        'location_specification' => "/$tableName/$idRow/$key/",
                        'existence_from' => '2019-03-01 00:00:00'
                    ]);
                }
            }
        }
        $olmpMan->commit();
    }

    public function loadCell($name)
    {
        $strucMan = $this->strucManager->database;
        $olmpMan = $this->olmpManager->database;
        $table = $strucMan->table($name);
        foreach ($table as $row) {
            //Debugger::barDump($row, 'row');
            $idColumn = "id_$name";
            //Debugger::barDump($idColumn, '$idColumn');
            //Debugger::barDump("/$name/$row->$idColumn/", 'location_specification');
            //Debugger::barDump($row->$idColumn, 'id');
            $id = $row->$idColumn;
            $parendId = $olmpMan->table('data_object')->where('location_specification', "/$name/$id/")->fetch()->object_id;
            //Debugger::barDump($parendId, '$parendId');
            foreach ($row as $key => $value) {
                //Debugger::barDump($key, 'key');
                //Debugger::barDump($value, 'value');
                $olmpMan->table('data_object')->insert([
                    'parent_object_id' => $parendId,
                    'location_specification' => "/$name/$id/$key/",
                    'existence_from' => '2019-03-01 00:00:00'
                ]);
            }
        }
    }

    public function calculateCellsSizes($name)
    {
        $strucMan = $this->strucManager->database;
        $olmpMan = $this->olmpManager->database;
        $table = $strucMan->table($name);
        foreach ($table as $row) {
            $idColumn = "id_$name";
            $id = $row->$idColumn;
            foreach ($row as $key => $value) {
                if ($value instanceof \DateInterval) {
                    $value = $value->format("%Y-%M-%D %H:%I:%S");
                }
                $length = strlen("$value");
                $olmpMan->table('data_object')->where('location_specification', "/$name/$id/$key/")->fetch()
                    ->update([
                        'size' => $length
                    ]);
                echo "updated value $value length " . strlen("$value") . "  <br>";
            }
        }
    }

    public function calculateSizes()
    {
        $strucMan = $this->strucManager->database;
        $olmpMan = $this->olmpManager->database;
        $uncalculated = $olmpMan->table('data_object')->where('size IS NULL AND  ROUND (   
        (
            LENGTH(location_specification)
            - LENGTH( REPLACE ( location_specification, "/", "") ) 
        ) / LENGTH("/")  = 4)');
        Debugger::barDump(count($uncalculated), 'uncalculated');
        foreach ($uncalculated as $row) {
            $location = $row->location_specification;
            preg_match('/\/(.*)\/(.*)\/(.*)\//', $location, $matches);
            //Debugger::barDump($matches, '$matches');
            $tableName = $matches[1];
            $id = $matches[2];
            $column = $matches[3];
            $value = $strucMan->table($tableName)
                ->get($id)
                ->$column;
            if ($value instanceof \DateInterval) {
                $value = $value->format("%Y-%M-%D %H:%I:%S");
            }
            $length = strlen("$value");
            $row->update([
                'size' => $length
            ]);
            echo $row->object_id . " updated size $length for $location - '$value'  <br>";
            continue;
        }
    }

    public function addDataReferences()
    {
        $strucMan = $this->strucManager->database;
        $olmpMan = $this->olmpManager->database;
        if ($this->structure == 1) {
            $this->addDataReferencesTableColumn('plavec', ['id_clovek' => 'clovek']);
            $this->addDataReferencesTableColumn('prihlaska', ['plavec' => 'plavec', 'id_skupina' => 'skupina']);
            $this->addDataReferencesTableColumn('skupina', ['id_termin' => 'termin', 'id_vykon_kategorie' => 'vykon_kategorie', 'id_vykon_kategorie' => 'vykon_kategorie']);
            $this->addDataReferencesTableColumn('termin', ['id_bazen' => 'bazen', 'id_obdobi' => 'obdobi']);
            //echo count($rows);
        }
    }

    public function addDataReferencesTableColumn(string $tabName, array $columns)
    {
        $strucMan = $this->strucManager->database;
        $olmpMan = $this->olmpManager->database;

        $rowsOfTable = $olmpMan->table('data_object')
            ->where("location_specification REGEXP ?", "^/$tabName/[0-9]+/$");
        foreach ($rowsOfTable as $row) {
            $location = $row->location_specification;
            $re = "/\/$tabName\/(\d+)\//";
            preg_match($re, $location, $matches, PREG_OFFSET_CAPTURE, 0);
            $dataRowId = $matches[1][0];
            //Debugger::barDump($matches, '$matches');
            //echo $dataRowId;
            $dataRow = $strucMan->table($tabName)->get($dataRowId);
            foreach ($columns as $column => $refTableName) {
                $this->addReference($column, $dataRow, $row, $refTableName);
            }
        }
    }

    public function addReference($columnName, $dataRow, $row, $refTableName)
    {
        $strucMan = $this->strucManager->database;
        $olmpMan = $this->olmpManager->database;
        $location = $row->location_specification;
        $rowId = $row->object_id;
        $dataRowColumn = $dataRow->$columnName;
        if ($dataRowColumn) {
            $refferedObject = $olmpMan->table('data_object')
                ->where("location_specification REGEXP ?", "/$refTableName/$dataRowColumn/")->fetch();
            if (!$refferedObject) {
                echo "/$columnName/$dataRowColumn/ not found<br>";
            }
            if (count($olmpMan->table('data_reference')->where('referrer_id=? AND reffered_id=?', $rowId, $refferedObject->object_id))) {
                echo $location . ' already inserted, continue<br>';
                return;
            }
            $olmpMan->table('data_reference')
                ->insert([
                    'referrer_id' => $rowId,
                    'reffered_id' => $refferedObject->object_id
                ]);
            echo $location . ' inserted<br>';
        } else {
            echo "$location $columnName null, continue<br>";
        }

    }

    public function calculateHigherLevelSizes($level)
    {
        $strucMan = $this->strucManager->database;
        $olmpMan = $this->olmpManager->database;
        $rows = $olmpMan->table('data_object')->where('size IS NULL AND  ROUND (   
        (
            LENGTH(location_specification)
            - LENGTH( REPLACE ( location_specification, "/", "") ) 
        ) / LENGTH("/")  = ?)', $level);
        //echo "calculateHigherLevelSizes level $level found uncalc " . count($rows)."<br>";
        foreach ($rows as $row) {
            $size = $olmpMan->table('data_object')
                ->where('parent_object_id', $row->object_id)
                ->sum('size');
            echo "size: $size <br>";
            $row->update(['size' => $size]);
        }
    }

    public function calculateHierarchicallity(array $tablesNames)
    {
        $strucMan = $this->strucManager->database;
        $olmpMan = $this->olmpManager->database;
        $root = $olmpMan->table('data_object')
            ->where("location_specification", "/")->fetch();
        $nonHierarchySizeFound = 0;
        $nonHierarchicalObjects = $olmpMan->query('
        SELECT referrer_id, COUNT(referrer_id) AS refference_count, SUM(data_object.size) AS object_size
        FROM data_reference
        LEFT JOIN data_object ON (data_reference.referrer_id=data_object.object_id)
        GROUP BY data_reference.referrer_id
        HAVING COUNT(referrer_id)>1
        ');
        foreach ($nonHierarchicalObjects as $row) {
            $nonHierarchySizeFound += $row->object_size;
        }
        $result = 1 - ($nonHierarchySizeFound / $root->size);
        $root->update(['hierarchicallity' => $result]);
        $olmpMan->table('data_object')->where('hierarchicallity IS NULL')
            ->update(['hierarchicallity' => 1]);

    }

    public function deleteDuplicates()
    {
        $olmpMan = $this->olmpManager->database;
        $strucMan = $this->strucManager->database;
        $olmpMan = $this->olmpManager->database;
        $query = "  DELETE do1 FROM data_object do1
                    INNER JOIN data_object do2 
                    WHERE 
                        do1.object_id < do2.object_id AND
                        do1.location_specification = do2.location_specification;";
        $olmpMan->query($query);
        echo "duplicates deleted<br>";
    }
}
