<?php

namespace App\Model;

use Nette;
use Nette\Database\Context;
use Nette\SmartObject;
use Tracy\Debugger;

class BaseManager {

    use SmartObject;

    /** @var Context */
    public $database;

    public function __construct(Context $default) {
        $this->database = $default;
    }
}