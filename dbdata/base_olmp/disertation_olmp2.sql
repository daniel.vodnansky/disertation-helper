-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Počítač: db:3306
-- Vytvořeno: Pát 20. bře 2020, 21:23
-- Verze serveru: 5.6.40
-- Verze PHP: 7.2.19

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Databáze: `disertation_olmp2`
--

-- --------------------------------------------------------

--
-- Struktura tabulky `data_object`
--

CREATE TABLE `data_object` (
  `object_id` int(11) NOT NULL,
  `parent_object_id` int(11) DEFAULT NULL,
  `size` int(11) DEFAULT NULL,
  `structuredness` float DEFAULT NULL,
  `hierarchicallity` float DEFAULT NULL,
  `information_amount` float DEFAULT NULL,
  `location_specification` text COLLATE utf8mb4_bin,
  `existence_from` datetime DEFAULT NULL,
  `existence_to` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

-- --------------------------------------------------------

--
-- Struktura tabulky `data_reference`
--

CREATE TABLE `data_reference` (
  `referrer_id` int(11) NOT NULL,
  `reffered_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;

--
-- Klíče pro exportované tabulky
--

--
-- Klíče pro tabulku `data_object`
--
ALTER TABLE `data_object`
  ADD PRIMARY KEY (`object_id`),
  ADD KEY `parent_object_id` (`parent_object_id`);

--
-- Klíče pro tabulku `data_reference`
--
ALTER TABLE `data_reference`
  ADD KEY `referrer_id` (`referrer_id`,`reffered_id`),
  ADD KEY `referred` (`reffered_id`);

--
-- AUTO_INCREMENT pro tabulky
--

--
-- AUTO_INCREMENT pro tabulku `data_object`
--
ALTER TABLE `data_object`
  MODIFY `object_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Omezení pro exportované tabulky
--

--
-- Omezení pro tabulku `data_object`
--
ALTER TABLE `data_object`
  ADD CONSTRAINT `parent` FOREIGN KEY (`parent_object_id`) REFERENCES `data_object` (`object_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Omezení pro tabulku `data_reference`
--
ALTER TABLE `data_reference`
  ADD CONSTRAINT `referred` FOREIGN KEY (`reffered_id`) REFERENCES `data_object` (`object_id`),
  ADD CONSTRAINT `referrer` FOREIGN KEY (`referrer_id`) REFERENCES `data_object` (`object_id`);
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
