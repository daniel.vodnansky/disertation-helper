configure \app\config\local.neon DB connection, if nescesarry or use dockerized MySQL and keep configuration<br>
run `docker-compose up`<br>
open database on http://localhost:8082/<br>
run SQL \dbdata\base_olmp\disertation_olmp1.sql AND/OR \dbdata\base_olmp\disertation_olmp2.sql to create empty metadata storage<br>
run SQL \dbdata\disertation_struc1 (1).sql AND/OR \dbdata\disertation_struc2 (2).sql to test it on my data or create yours<br>
go to app/Presenters/HomepagePresenter.php and <br>
<ul>
    <li>update  `$this->tablesNames`, if structure is different</li>
    <li>set  `public $structure` to use first or second dataset</li>
    <li>update `public function addDataReference` if foreign keys exist and are different</li>
</ul>    
run http://localhost:8080/ to calculate<br>
<br>

**dbdata/disertation (2).sql** includes full SQL data